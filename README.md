# msgdemo

A small demonstration of LENS messengers (`coreapi::sysobj::messenger`) and object flows (`objectflows`). The `msgdemo_server` program serves a socket at `/tmp/lens-msgserver`, receives a `Widget` structure from it, and prints fields from it. The `msgdemo_client` program connects to the socket and sends such a structure.
