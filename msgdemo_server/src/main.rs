// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#![no_std]
#![no_main]
extern crate alloc;
extern crate lens_flows;
extern crate lens_files;
extern crate postcardflows;
extern crate serde;

use alloc::string::String;
use lens_flows::byte::Write;
use lens_flows::object::Receive;
use postcardflows::PostcardReceiveFilter;
use serde::Deserialize;

#[derive(Deserialize)]
struct Widget {
    manufacturer: String,
    model: String,
}

#[no_mangle]
fn main() {
    let stdout =
        lens_files::File::from_descriptor(1).expect("Failed to open stdout");
    let svr = lens_messenger::Server::new("/tmp/lens-msgserver")
        .expect("Failed to create server");
    let ep = svr.accept().expect("Failed to accept connection");
    let flow = PostcardReceiveFilter::new(&ep);
    let widget: Widget = flow.receive().expect("Failed to receive message");

    stdout.write(b"A ").expect("Failed to write stdout");
    stdout
        .write(widget.model.as_bytes())
        .expect("Failed to write stdout");
    stdout.write(b" made by ").expect("Failed to write stdout");
    stdout
        .write(widget.manufacturer.as_bytes())
        .expect("Failed to write stdout");
    stdout
        .write(b" was received.\n")
        .expect("Failed to write stdout");
}
